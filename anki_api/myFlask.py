#!/usr/bin/python
# -*- coding: utf-8 -*-
from flask import Flask, jsonify, render_template
from flask import request
import mysql.connector
from mysql.connector.cursor import MySQLCursorPrepared
app = Flask(__name__)

db = mysql.connector.connect(
    host='localhost',
    user='root',
    passwd='root',
    database='jpod'
)
conn = db.cursor(cursor_class=MySQLCursorPrepared)

# this is the golden root: https://s3.amazonaws.com/cdn.innovativelanguage.com/japanesepod101
# example: https://s3.amazonaws.com/cdn.innovativelanguage.com/japanesepod101/learningcenter/audio/transcript_56/1.mp3
@app.route('/')
def hello_world():
    return render_template('ankiAdd.html')

@app.route('/lesson/<lesson_num>')
def lesson(lesson_num):

    sql = '''
        SELECT 
	CONCAT(season, ", Lesson ", lesson_num) AS lesson,
            dialogue.id,
            sound,
            expression,
            hiragana,
            english
        FROM dialogue
        LEFT JOIN lesson ON lesson.id = dialogue.lesson_id
        WHERE season = 'Intermediate Season 1'
        AND lesson_num = ?
    '''
    conn.execute(sql, (lesson_num,))
    result = conn.fetchall()

    payload = []
    for row in result:
        payload.append({'lesson': row[0].decode('utf-8'), 'id': row[1], 'sound': row[2].decode('utf-8'), 'kanji': row[3].decode('utf-8'), 'hiragana': row[4].decode('utf-8'), 'english': row[5].decode('utf-8')})

    return render_template('ankiAdd.html', payload=payload)
    #return jsonify(payload)

app.run(debug=True, port=8000)
