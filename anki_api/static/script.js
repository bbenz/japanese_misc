let endpoint = 'http://localhost:8765/';
function ankify(id) {
  let kanji = document.querySelector(`#row_${id} .kanji`).innerText;
  let hiragana = document.querySelector(`#row_${id} .hiragana`).innerText;
  let english = document.querySelector(`#row_${id} .english`).innerText;
  let soundUrl = document.querySelector(`#audio_${id}`).src;
  let lesson = document.querySelector('.lesson').innerText;
  let soundfilename = id + '_' + lesson.split(' ').join('_').replace(/,/g, '');
  data = {
    "action": "addNote",
    "version": 6,
    "params": {
      "note": {
        "deckName": "Reading",
        "modelName": "reading_cards",
        "fields": {
          "kanji": kanji,
          "kana": hiragana,
          "english": english,
          "sound": "",
          "dialogue_id": id.toString(),
          "lesson": lesson
        },
        "options": {
          "allowDuplicate": false
        },
        "tags": [
          "jscrape"
        ],
        "audio": {
          "url": soundUrl,
          "filename": soundfilename,
          "skipHash": "7e2c2f954ef6051373ba916f000168dc",
          "fields": [
            "sound"
          ]
        }
      }
    }
  }
  json = JSON.stringify(data);
  $.ajax({
    url: endpoint,
    method: 'POST',
    data: json,
    success: function(response) {
      console.log(response);
    }
  });

}


