select constit_id, count, mnem, primitive
from
(select constit_id, count(*) AS count from note_constit group by constit_id order by count(*) desc) AS temp
left join constit on temp.constit_id = constit.id;