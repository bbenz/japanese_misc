import csv
import mysql.connector
from mysql.connector.cursor import MySQLCursorPrepared

db = mysql.connector.connect(
    host='localhost',
    user='root',
    passwd='root',
    database='kanjidic'
)
conn = db.cursor(cursor_class=MySQLCursorPrepared)
sql = '''
    UPDATE kanji
    SET heisig_keyword = ?
    WHERE heisig_id = ?
'''    

with open('heisig.csv') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        rtk_index = row[4]
        if rtk_index.isdigit():
            rtk_index = int(rtk_index)
            kanji = row[0]
            keyword = row[1]
            conn.execute(sql, (keyword, rtk_index))
db.commit()


