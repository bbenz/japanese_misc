import xml.etree.ElementTree as ET
import sys
import mysql.connector
from entitymap import eMap
from create_senses_tables import senses
from mysql.connector.cursor import MySQLCursorPrepared

print('parsing the xml file')
root = ET.parse('./kanjidict2.xml').getroot()

db = mysql.connector.connect(
    host='localhost',
    user='root',
    passwd='root',
    database='kanjidic'
)
conn = db.cursor(cursor_class=MySQLCursorPrepared)


conn.execute('TRUNCATE kanji')
db.commit()
kanji_sql = '''
    INSERT IGNORE INTO kanji
        (id, literal, heisig_id, grade, stroke_count, freq, rad_name, old_jlpt, single_read, meaning)
    VALUES
        ( ?,       ?,         ?,     ?,            ?,    ?,        ?,    ?,           ?,       ?)
'''
kanji_values = []

reading_sql = '''
    INSERT IGNORE INTO reading
        (kanji_id, origin, reading)
    VALUES
        (?, ?, ?)
'''

# this is for adding all readings to db
all_reading_values = []

print('looping through each entry')
i = 0
for entry in root.findall('character'):
    i += 1
    entry = root[i]
    literal = entry.find('literal').text
    english = []
    # this is for finding single_read kanji
    entry_reading_values = []

    # get heisig index no. (if there is one)
    dic_number = entry.find('dic_number')
    heisig_index = None
    if dic_number is not None:
        # get heisig index
        for dic_ref in list(entry.find('dic_number').findall('dic_ref')):
            dr_type = dic_ref.attrib.get('dr_type')
            if dr_type == 'heisig6':
                heisig_index = dic_ref.text

    # get misc
    misc = entry.find('misc')
    if misc is not None:
        grade = misc.find('grade').text if (misc.find('grade') is not None) else None
        stroke_count = misc.find('stroke_count').text  if (misc.find('stroke_count') is not None) else None # this only gets the first entry (which is the official accepted stroke count anyways)
        freq = misc.find('freq').text if (misc.find('freq') is not None) else None
        rad_name = misc.find('rad_name').text if (misc.find('rad_name') is not None) else None
        jlpt = misc.find('jlpt').text if (misc.find('jlpt') is not None) else None

    # do reading related stuff
    reading_meaning = entry.find('reading_meaning')
    if reading_meaning is not None:
        # get nanori
        for nanori in list(entry.find('reading_meaning').findall('nanori')):
            print(nanori.text)
            all_reading_values.append((i, 'nanori', nanori.text))
            entry_reading_values.append(('nanori', nanori.text))


        # meaning groups (readings and meanings)
        for meaning_group in list(entry.find('reading_meaning').findall('rmgroup')):
            # readings
            for reading in list(meaning_group.findall('reading')):
                # on
                if reading.attrib.get('r_type') == 'ja_on':
                    all_reading_values.append((i, 'on', reading.text))
                    entry_reading_values.append(('on', reading.text))
                # kun
                if reading.attrib.get('r_type') == 'ja_kun':
                    all_reading_values.append((i, 'kun', reading.text))
                    entry_reading_values.append(('kun', reading.text))
            # meanings
            for meaning in list(meaning_group.findall('meaning')):
                if meaning.attrib.get('m_lang') is None:
                    english.append(meaning.text)




    single_read = entry_reading_values[0][1] if (len(entry_reading_values) == 1) else None
    meaning = ', '.join(english)
    values = (i, literal, heisig_index, grade, stroke_count, freq, rad_name, jlpt, single_read, meaning)
    kanji_values.append(values)
    # we don't need to do any appending for the all_reading_values, that's already taken care of during the loop
    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')


print('submitting values to DB')
conn.executemany(kanji_sql, kanji_values)
conn.executemany(reading_sql, all_reading_values)
print('committing values to db')
db.commit()
sys.exit()

for x in range(1, 50000):
    ent_seq = root[x].find('ent_seq').text
    # set children of kanji element
    for k_ele in list(root[x].findall('k_ele')):
        keb = k_ele.find('keb').text
        k_ele_values.append((ent_seq, keb))
        for ke_inf in list(k_ele.findall('ke_inf')):
            text = eMap[ke_inf.text]
            ke_inf_values.append((ent_seq, keb, text))
        for ke_pri in list(k_ele.findall('ke_pri')):
            ke_pri_values.append((ent_seq, keb, ke_pri.text))

    for r_ele in list(root[x].findall('r_ele')):
        reb = r_ele.find('reb').text
        re_nokanji = 0 if (r_ele.find('re_nokanji') is None) else 1
        r_ele_values.append((ent_seq, reb, re_nokanji))
        for re_restr in list(r_ele.findall('re_restr')):
            re_restr_values.append((ent_seq, reb, re_restr.text))
        for re_inf in list(r_ele.findall('re_inf')):
            text = eMap[re_inf.text]
            re_inf_values.append((ent_seq, reb, text))
        for re_pri in list(r_ele.findall('re_pri')):
            re_pri_values.append((ent_seq, reb, re_pri.text))

    sense_ord = 0
    for sense_xml_elem in list(root[x].findall('sense')):
        for s in senses:
            for child_elem in list(sense_xml_elem.findall(s)):
                sense_txt = child_elem.text
                if s in expand_text and sense_txt in eMap:
                    sense_txt = eMap[sense_txt]
                senses_values[s].append((ent_seq, sense_ord, sense_txt))
        # lsource is pretty unique, it's not in "senses" even though it's a child of "senses"
        for lsource in list(sense_xml_elem.findall('lsource')):
            lang = lsource.attrib.get('{http://www.w3.org/XML/1998/namespace}lang')
            ls_type = lsource.attrib.get('ls_type')
            ls_wasei = lsource.attrib.get('ls_wasei')
            lsource_values.append((ent_seq, sense_ord, lsource.text, lang, ls_type, ls_wasei))
        # gloss also has an attribute we want to grab
        for gloss in list(sense_xml_elem.findall('gloss')):
            g_type = gloss.attrib.get('g_type')
            gloss_values.append((ent_seq, sense_ord, gloss.text, g_type))




        sense_ord = sense_ord + 1


conn.executemany(k_ele_sql, k_ele_values)
conn.executemany(ke_inf_sql, ke_inf_values)
conn.executemany(ke_pri_sql, ke_pri_values)

conn.executemany(r_ele_sql, r_ele_values)
conn.executemany(re_restr_sql, re_restr_values)
conn.executemany(re_inf_sql, re_inf_values)
conn.executemany(re_pri_sql, re_pri_values)

# senses are complicated
for s in senses:
    conn.executemany(senses_sql[s], senses_values[s])

conn.executemany(lsource_sql, lsource_values)
conn.executemany(gloss_sql, gloss_values)

db.commit()
