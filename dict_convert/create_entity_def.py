import mysql.connector
from entitymap import eMap
from mysql.connector.cursor import MySQLCursorPrepared

db = mysql.connector.connect(
    host='localhost',
    user='root',
    passwd='root',
    database='jmdict'
)
conn = db.cursor(cursor_class=MySQLCursorPrepared)

table_def = '''
CREATE TABLE IF NOT EXISTS `entity_def` (
  `abbrev` varchar(255) NOT NULL,
  `def` varchar(255) NOT NULL,
  PRIMARY KEY (`abbrev`,`def`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
'''
conn.execute(table_def)

sql = 'INSERT IGNORE INTO entity_def (def, abbrev) VALUES (?, ?)'
vals = []
for key in eMap.keys():
    vals.append((key, eMap[key]))

conn.executemany(sql, vals)
db.commit()
print(vals)
