import mysql.connector
from mysql.connector.cursor import MySQLCursorPrepared

db = mysql.connector.connect(
    host='localhost',
    user='root',
    passwd='root',
    database='jmdict'
)
conn = db.cursor(cursor_class=MySQLCursorPrepared)

senses = [
    "stagk",
    "stagr",
    "pos",
    "xref",
    "ant",
    "field",
    "misc",
    "s_info",
    "dial"
]

# lsource is a special case
conn.execute('DROP TABLE IF EXISTS lsource')
table_def = '''
CREATE TABLE IF NOT EXISTS `lsource` (
`ent_seq` varchar(255) NOT NULL,
`sense_ord` INT(8) NOT NULL,
`lsource` varchar(255) NOT NULL,
`lang` varchar(8),
`ls_type` varchar(255),
`ls_wasei` varchar(255),
PRIMARY KEY (`ent_seq`,`sense_ord`,`lsource`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
'''
conn.execute(table_def)

# gloss also has potential extra fields
conn.execute('DROP TABLE IF EXISTS gloss')
table_def = '''
CREATE TABLE IF NOT EXISTS `gloss` (
`ent_seq` varchar(255) NOT NULL,
`sense_ord` INT(8) NOT NULL,
`gloss` varchar(255) NOT NULL,
`g_type` varchar(255),
PRIMARY KEY (`ent_seq`,`sense_ord`,`gloss`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
'''
conn.execute(table_def)


for sense in senses:
    table_def = 'DROP TABLE IF EXISTS {}'.format(sense)
    conn.execute(table_def)

for sense in senses:
    table_def = '''
    CREATE TABLE IF NOT EXISTS `{}` (
    `ent_seq` varchar(255) NOT NULL,
    `sense_ord` INT(8) NOT NULL,
    `{}` varchar(255) NOT NULL,
    PRIMARY KEY (`ent_seq`,`{}`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    '''.format(sense, sense, sense)
    conn.execute(table_def)

db.commit()
