import csv
import sys
import mysql.connector
from mysql.connector.cursor import MySQLCursorPrepared

db = mysql.connector.connect(
    host='localhost',
    user='root',
    passwd='root',
    database='kanjidic'
)
conn = db.cursor(cursor_class=MySQLCursorPrepared)
constit_sql = 'INSERT IGNORE INTO constit (mnem) VALUES (?)'

constit_values = []
with open('heisig_4e.csv') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        rtk_index = row[4]
        if rtk_index.isdigit() and rtk_index != '9999':
            rtk_index = int(rtk_index)
            kanji = row[0]
            keyword = row[1]
            constits = row[19]
            constits = constits.strip(',') # remove leading and trailing ,
            for constit in constits.split(','):
                if (constit.strip(),) not in constit_values:
                    constit_values.append((constit.strip(),))
print('adding items to constit table')
# conn.executemany(constit_sql, constit_values)
# db.commit()

print('creating dictionary')
sql = 'SELECT * FROM kanjidic.constit'
conn.execute(sql)
result = conn.fetchall()
constit_dic = {}
for row in result:
    mnem = row[1].decode('utf-8')
    id = row[0]
    constit_dic[mnem.lower()] = id

print('cycling through entries')
kanji_constit = []
with open('heisig_4e.csv') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        rtk_index = row[4]
        if rtk_index.isdigit() and rtk_index != '9999':
            kanji = row[0]
            constits = row[19]
            constits = constits.strip(',') # remove leading and trailing ,
            i = 0
            for constit in constits.split(','):
                constit = constit.strip()
                kanji_constit.append((kanji, i, constit_dic[constit.lower()]))
                i += 1
sql = 'INSERT IGNORE INTO kanji_constit (kanji, seq, constit_id) VALUES (?, ?, ?)'
conn.executemany(sql, kanji_constit)
db.commit()
