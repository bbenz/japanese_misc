import csv
import mysql.connector
from mysql.connector.cursor import MySQLCursorPrepared

db = mysql.connector.connect(
    host='localhost',
    user='root',
    passwd='root',
    database='kanjidic'
)
conn = db.cursor(cursor_class=MySQLCursorPrepared)
sql = '''
    UPDATE kanji
    SET heisig_keyword = ?, joyo = ?
    WHERE heisig_id = ?
'''    

with open('heisig_6e.csv') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        rtk_index = row[3]
        if rtk_index.isdigit() and rtk_index != '9999':
            rtk_index = int(rtk_index)
            kanji = row[1]
            keyword = row[5]
            joyo = 1 if row[7] == 'yes' else 0
            print(rtk_index, kanji, keyword, joyo)
            conn.execute(sql, (keyword, joyo, rtk_index))
db.commit()

