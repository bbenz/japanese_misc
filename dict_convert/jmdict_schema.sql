-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: jmdict
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.18.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ant`
--

DROP TABLE IF EXISTS `ant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ant` (
  `ent_seq` varchar(255) NOT NULL,
  `sense_ord` int(8) NOT NULL,
  `ant` varchar(255) NOT NULL,
  PRIMARY KEY (`ent_seq`,`ant`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dial`
--

DROP TABLE IF EXISTS `dial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dial` (
  `ent_seq` varchar(255) NOT NULL,
  `sense_ord` int(8) NOT NULL,
  `dial` varchar(255) NOT NULL,
  PRIMARY KEY (`ent_seq`,`dial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `entity_def`
--

DROP TABLE IF EXISTS `entity_def`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entity_def` (
  `abbrev` varchar(255) NOT NULL,
  `def` varchar(255) NOT NULL,
  PRIMARY KEY (`abbrev`,`def`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `field`
--

DROP TABLE IF EXISTS `field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field` (
  `ent_seq` varchar(255) NOT NULL,
  `sense_ord` int(8) NOT NULL,
  `field` varchar(255) NOT NULL,
  PRIMARY KEY (`ent_seq`,`field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gloss`
--

DROP TABLE IF EXISTS `gloss`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gloss` (
  `ent_seq` varchar(255) NOT NULL,
  `sense_ord` int(8) NOT NULL,
  `gloss` varchar(255) NOT NULL,
  `g_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ent_seq`,`sense_ord`,`gloss`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `k_ele`
--

DROP TABLE IF EXISTS `k_ele`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `k_ele` (
  `ent_seq` varchar(45) NOT NULL,
  `keb` varchar(255) NOT NULL,
  PRIMARY KEY (`ent_seq`,`keb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ke_inf`
--

DROP TABLE IF EXISTS `ke_inf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ke_inf` (
  `ent_seq` varchar(45) NOT NULL,
  `keb` varchar(255) NOT NULL,
  `ke_inf` varchar(255) NOT NULL,
  PRIMARY KEY (`ent_seq`,`keb`,`ke_inf`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ke_pri`
--

DROP TABLE IF EXISTS `ke_pri`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ke_pri` (
  `ent_seq` varchar(45) NOT NULL,
  `keb` varchar(255) NOT NULL,
  `ke_pri` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ent_seq`,`keb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lsource`
--

DROP TABLE IF EXISTS `lsource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lsource` (
  `ent_seq` varchar(255) NOT NULL,
  `sense_ord` int(8) NOT NULL,
  `lsource` varchar(255) NOT NULL,
  `lang` varchar(8) DEFAULT NULL,
  `ls_type` varchar(255) DEFAULT NULL,
  `ls_wasei` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ent_seq`,`sense_ord`,`lsource`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `misc`
--

DROP TABLE IF EXISTS `misc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `misc` (
  `ent_seq` varchar(255) NOT NULL,
  `sense_ord` int(8) NOT NULL,
  `misc` varchar(255) NOT NULL,
  PRIMARY KEY (`ent_seq`,`misc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pos`
--

DROP TABLE IF EXISTS `pos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pos` (
  `ent_seq` varchar(255) NOT NULL,
  `sense_ord` int(8) NOT NULL,
  `pos` varchar(255) NOT NULL,
  PRIMARY KEY (`ent_seq`,`pos`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `r_ele`
--

DROP TABLE IF EXISTS `r_ele`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `r_ele` (
  `ent_seq` varchar(20) NOT NULL,
  `reb` varchar(255) NOT NULL,
  `re_nokanji` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`ent_seq`,`reb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `re_inf`
--

DROP TABLE IF EXISTS `re_inf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `re_inf` (
  `ent_seq` varchar(45) NOT NULL,
  `reb` varchar(255) NOT NULL,
  `re_inf` varchar(255) NOT NULL,
  PRIMARY KEY (`ent_seq`,`reb`,`re_inf`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `re_pri`
--

DROP TABLE IF EXISTS `re_pri`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `re_pri` (
  `ent_seq` varchar(45) NOT NULL,
  `reb` varchar(255) NOT NULL,
  `re_pri` varchar(255) NOT NULL,
  PRIMARY KEY (`ent_seq`,`reb`,`re_pri`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `re_restr`
--

DROP TABLE IF EXISTS `re_restr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `re_restr` (
  `ent_seq` varchar(45) NOT NULL,
  `reb` varchar(255) NOT NULL,
  `re_restr` varchar(255) NOT NULL,
  PRIMARY KEY (`ent_seq`,`reb`,`re_restr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_info`
--

DROP TABLE IF EXISTS `s_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_info` (
  `ent_seq` varchar(255) NOT NULL,
  `sense_ord` int(8) NOT NULL,
  `s_info` varchar(255) NOT NULL,
  PRIMARY KEY (`ent_seq`,`s_info`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stagk`
--

DROP TABLE IF EXISTS `stagk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stagk` (
  `ent_seq` varchar(255) NOT NULL,
  `sense_ord` int(8) NOT NULL,
  `stagk` varchar(255) NOT NULL,
  PRIMARY KEY (`ent_seq`,`stagk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stagr`
--

DROP TABLE IF EXISTS `stagr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stagr` (
  `ent_seq` varchar(255) NOT NULL,
  `sense_ord` int(8) NOT NULL,
  `stagr` varchar(255) NOT NULL,
  PRIMARY KEY (`ent_seq`,`stagr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xref`
--

DROP TABLE IF EXISTS `xref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xref` (
  `ent_seq` varchar(255) NOT NULL,
  `sense_ord` int(8) NOT NULL,
  `xref` varchar(255) NOT NULL,
  PRIMARY KEY (`ent_seq`,`xref`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-20 12:21:53
