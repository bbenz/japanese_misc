import xml.etree.ElementTree as ET
import mysql.connector
from entitymap import eMap
from create_senses_tables import senses
from mysql.connector.cursor import MySQLCursorPrepared
root = ET.parse('../../Downloads/JMdict_e').getroot()

db = mysql.connector.connect(
    host='localhost',
    user='root',
    passwd='root',
    database='jmdict'
)
conn = db.cursor(cursor_class=MySQLCursorPrepared)

expand_text = ['pos', 'field', 'misc', 'dial']

k_ele_sql = 'INSERT IGNORE INTO k_ele (ent_seq, keb) VALUES (?, ?)'
k_ele_values = []
ke_inf_sql = 'INSERT IGNORE INTO ke_inf (ent_seq, keb, ke_inf) VALUES (?, ?, ?)'
ke_inf_values = []
ke_pri_sql = 'INSERT IGNORE INTO ke_pri (ent_seq, keb, ke_pri) VALUES (?, ?, ?)'
ke_pri_values = []

r_ele_sql = 'INSERT IGNORE INTO r_ele (ent_seq, reb, re_nokanji) VALUES (?, ?, ?)'
r_ele_values = []
re_restr_sql = 'INSERT IGNORE INTO re_restr (ent_seq, reb, re_restr) VALUES (?, ?, ?)'
re_restr_values = []
re_inf_sql = 'INSERT IGNORE INTO re_inf (ent_seq, reb, re_inf) VALUES (?, ?, ?)'
re_inf_values = []
re_pri_sql = 'INSERT IGNORE INTO re_pri (ent_seq, reb, re_pri) VALUES (?, ?, ?)'
re_pri_values = []

senses_sql = {}
for s in senses:
    senses_sql[s] = 'INSERT IGNORE INTO {} (ent_seq, sense_ord, {}) VALUES (?, ?, ?)'.format(s, s)
senses_values = {}
for s in senses:
    senses_values[s] = []

lsource_sql = 'INSERT IGNORE INTO lsource (ent_seq, sense_ord, lsource, lang, ls_type, ls_wasei) VALUES (?, ?, ?, ?, ?, ?)'
lsource_values = []

gloss_sql = 'INSERT IGNORE INTO gloss (ent_seq, sense_ord, gloss, g_type) VALUES (?, ?, ?, ?)'
gloss_values = []

for entry in root:
    ent_seq = entry.find('ent_seq').text
    # set children of kanji element
    for k_ele in list(entry.findall('k_ele')):
        keb = k_ele.find('keb').text
        k_ele_values.append((ent_seq, keb))
        for ke_inf in list(k_ele.findall('ke_inf')):
            text = eMap[ke_inf.text]
            ke_inf_values.append((ent_seq, keb, text))
        for ke_pri in list(k_ele.findall('ke_pri')):
            ke_pri_values.append((ent_seq, keb, ke_pri.text))

    for r_ele in list(entry.findall('r_ele')):
        reb = r_ele.find('reb').text
        re_nokanji = 0 if (r_ele.find('re_nokanji') is None) else 1
        r_ele_values.append((ent_seq, reb, re_nokanji))
        for re_restr in list(r_ele.findall('re_restr')):
            re_restr_values.append((ent_seq, reb, re_restr.text))
        for re_inf in list(r_ele.findall('re_inf')):
            text = eMap[re_inf.text]
            re_inf_values.append((ent_seq, reb, text))
        for re_pri in list(r_ele.findall('re_pri')):
            re_pri_values.append((ent_seq, reb, re_pri.text))

    sense_ord = 0
    for sense_xml_elem in list(entry.findall('sense')):
        for s in senses:
            for child_elem in list(sense_xml_elem.findall(s)):
                sense_txt = child_elem.text
                if s in expand_text and sense_txt in eMap:
                    sense_txt = eMap[sense_txt]
                senses_values[s].append((ent_seq, sense_ord, sense_txt))
        # lsource is pretty unique, it's not in "senses" even though it's a child of "senses"
        for lsource in list(sense_xml_elem.findall('lsource')):
            lang = lsource.attrib.get('{http://www.w3.org/XML/1998/namespace}lang')
            ls_type = lsource.attrib.get('ls_type')
            ls_wasei = lsource.attrib.get('ls_wasei')
            lsource_values.append((ent_seq, sense_ord, lsource.text, lang, ls_type, ls_wasei))
        # gloss also has an attribute we want to grab
        for gloss in list(sense_xml_elem.findall('gloss')):
            g_type = gloss.attrib.get('g_type')
            gloss_values.append((ent_seq, sense_ord, gloss.text, g_type))
        sense_ord = sense_ord + 1    


conn.executemany(k_ele_sql, k_ele_values)
conn.executemany(ke_inf_sql, ke_inf_values)
conn.executemany(ke_pri_sql, ke_pri_values)

conn.executemany(r_ele_sql, r_ele_values)
conn.executemany(re_restr_sql, re_restr_values)
conn.executemany(re_inf_sql, re_inf_values)
conn.executemany(re_pri_sql, re_pri_values)

# senses are complicated
for s in senses:
    conn.executemany(senses_sql[s], senses_values[s])

conn.executemany(lsource_sql, lsource_values)
conn.executemany(gloss_sql, gloss_values)

db.commit()
