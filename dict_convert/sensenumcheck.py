import xml.etree.ElementTree as ET
from entitymap import eMap
from mysql.connector.cursor import MySQLCursorPrepared
root = ET.parse('../../Downloads/JMdict_e').getroot()


for x in range(1, 5000):
    ent_seq = root[x].find('ent_seq').text
    for sense in list(root[x].findall('sense')):
        lsource_i = 0
        for lsource in list(sense.findall('lsource')):
            lang = lsource.attrib.get('{http://www.w3.org/XML/1998/namespace}lang')
            ls_type = lsource.attrib.get('ls_type')
            ls_wasei = lsource.attrib.get('ls_wasei')
            if lsource_i > 0:
                print(ent_seq, lsource_i, lsource.text, lang, ls_type, ls_wasei)
            lsource_i = lsource_i + 1
