#/usr/bin/python
# -*- coding: utf-8 -*-
import MySQLdb
import mysql.connector
from mysql.connector.cursor import MySQLCursorPrepared


db = MySQLdb.connect(
    user='root',
    passwd='root',
    db='nihongoShark',
    charset='utf8'
)


dbwrite = mysql.connector.connect(
    host='localhost',
    user='root',
    passwd='root',
    database='nihongoShark'
)
conn = dbwrite.cursor(cursor_class=MySQLCursorPrepared)


def getKanjiKeywordDic():
    sql = '''
        SELECT kanji, keyword
        FROM note
    '''
    conn.execute(sql)
    result = conn.fetchall()
    payload = {}
    for row in result:
        kanji = row[0].decode('utf-8')
        keyword = row[1].decode('utf-8')
        payload[kanji] = keyword
    return payload

def getKanjiIdDic():
    sql = '''
        SELECT kanji, id
        FROM note
    '''
    db.query(sql)
    result = db.store_result()
    myDic = result.fetch_row(maxrows=0, how=1) # return dictionary, key=column
    payload = {}
    for row in myDic:
        payload[row['kanji']] = row['id']
    return payload

# get the list of note expressions
sql = '''
    SELECT 
    id, expression
    FROM iKnow.note
'''
db.query(sql)
result = db.store_result()
noteTable = result.fetch_row(maxrows=0, how=1) # return dictionary, key=column

# some write statements to prepare, for the looping
note_heisig_sql = 'INSERT IGNORE INTO iKnow.note_heisig (note_id, heisig_id) VALUES (?, ?)'
# note heisig_max insert
heisig_max_sql = 'UPDATE IGNORE iKnow.note SET heisig_max_id = ? WHERE id = ?'

kanjiIdRef = getKanjiIdDic()
for row in noteTable:
    max_num = 0
    for ji in row['expression']:
        if ji in kanjiIdRef:
            heisig_id = kanjiIdRef[ji]
            if heisig_id > max_num:
                max_num = heisig_id
            values = (row['id'], heisig_id)
            print(values)
            conn.execute(note_heisig_sql, values)
    print('super max:', max_num)
    values = (max_num, row['id'])
    conn.execute(heisig_max_sql, values)
dbwrite.commit()
