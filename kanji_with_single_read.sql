SELECT * FROM (SELECT count(*) AS count, jk.ent_seq, kanji, keb, n.id, reb, pos
FROM jmdict.k_ele jk
RIGHT JOIN nihongoShark.note n ON jk.keb = n.kanji
JOIN jmdict.r_ele jr ON jk.ent_seq = jr.ent_seq
LEFT JOIN jmdict.pos p ON jk.ent_seq = p.ent_seq
GROUP BY kanji
order by id) derived
WHERE count = 1
AND pos != 'n'
ORDER BY id;