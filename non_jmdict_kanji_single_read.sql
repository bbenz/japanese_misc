USE nihongoShark;

SELECT
	subtable.note_id,
	note.kanji,
	r.reading
FROM 
	(
	SELECT note_id, COUNT(*) AS count 
    FROM reading 
    GROUP BY note_id
    ) AS subtable
LEFT JOIN note ON subtable.note_id = note.id
LEFT JOIN reading r ON subtable.note_id = r.note_id
WHERE r.reading NOT LIKE '%.%'
AND subtable.count = 1
ORDER BY note_id
;

select 
	constituent,
    kanji
from constituent c
left join note n on c.constituent = n.keyword
WHERE note_id = 35
ORDER BY seq;

select 