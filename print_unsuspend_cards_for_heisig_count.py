#!/usr/bin/python
# -*- coding: utf-8 -*-
import mysql.connector
import MySQLdb
from mysql.connector.cursor import MySQLCursorPrepared
import json

# MySQL
dbwrite = mysql.connector.connect(
    host='localhost',
    user='root',
    passwd='root',
    database='iKnow'
)
conn = dbwrite.cursor(cursor_class=MySQLCursorPrepared)

db = MySQLdb.connect(
    user='root',
    passwd='root',
    db='iKnow',
    charset='utf8'
)


# this sql will collect Core 6k deck cards for unsuspension
sql = '''
    SELECT c1.id, c1.ord
    FROM cards c1
    LEFT JOIN cards c2 ON c1.nid = c2.nid AND c1.ord < c2.ord
    WHERE c2.nid IS NULL
    AND c1.nid in (
            SELECT meta_id FROM iKnow.note
            WHERE heisig_max_id BETWEEN 1459 and 1547
            AND heisig_max_id != 0
            ORDER BY heisig_max_id
    )
    ORDER BY c1.nid;
'''

db.query(sql)
result = db.store_result()
noteTable = result.fetch_row(maxrows=0, how=1) # return dictionary, key=column
print(len(noteTable))
idArray = []
for row in noteTable:
    idArray.append(row['id'])
    print(row['id'], end=', ')

import http.client

conn = http.client.HTTPConnection("localhost")

payload = json.dumps(idArray)
payload = idArray

headers = {
    'Content-Type': "application/json",
    'cache-control': "no-cache",
    'Postman-Token': "42766d3f-3910-4557-9ce7-00ae4040124b"
    }

conn.request("POST", "", payload, headers)
print(payload)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
