SELECT
	nn.id,
	nn.kanji,
  ikn.Reading,
  nn.jlpt,
  nn.keyword,
  ikn.Meaning
FROM nihongoShark.note nn
INNER JOIN iKnow.note ikn ON nn.kanji = ikn.Expression;
