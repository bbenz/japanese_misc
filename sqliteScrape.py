#!/usr/bin/python
# -*- coding: utf-8 -*-
import sqlite3
import mysql.connector
import re
from mysql.connector.cursor import MySQLCursorPrepared

# MySQL
db = mysql.connector.connect(
    host='localhost',
    user='root',
    passwd='root',
    database='iKnow'
)
conn = db.cursor(cursor_class=MySQLCursorPrepared)

# sqlite3
sqlitedb = sqlite3.connect('collection.anki2')
c = sqlitedb.cursor()

def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext

meta_sql = '''
    INSERT IGNORE INTO meta
        (id, guid, mid, `mod`, usn, tags, flds, sfld, csum, flags, data)
    VALUES
        (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
'''

note_sql = '''
    INSERT IGNORE INTO note
        (id, meta_id, Expression, Meaning, Reading, Audio, Image_URI, iKnowID, iKnowType, tag, Expression_html, Meaning_html, Reading_html)
    VALUES
        (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
'''

tag_sql = '''
    INSERT IGNORE INTO tag
        (note_id, tag)
    VALUES
        (?, ?)
'''

# 1342695938323
c.execute('SELECT * FROM notes WHERE mid IN (1342695938323, 1342695926185)')
id = 0
for row in c:
    meta_id = row[0]
    tag = row[5]

    # # Anki meta data for deck
    # conn.execute(meta_sql, row)

    note = row[6].split('\u001f')
    #print(note)
    expression_html = note[0]
    expression = cleanhtml(expression_html)
    meaning_html = note[1]
    meaning = cleanhtml(meaning_html)
    reading_html = note[2]
    reading = cleanhtml(reading_html)
    audio = note[3]
    image = note[4]
    iknow_id = note[5].split(':')[1]
    iknow_type = note[6]
    note_values = (id, meta_id, expression, meaning, reading, audio, image, iknow_id, iknow_type, tag, expression_html, meaning_html, reading_html)
    id += 1
    #print(note_values)
    # conn.execute(note_sql, note_values)

    # # anki deck tags
    # tags = row[5]
    # for tag in tags.strip().split(' '):
    #     tag_values = (note_id, tag.strip())
    #     conn.execute(tag_sql, tag_values)

    #singleRead = note[21] if note[21] is not '' else None

# db.commit()


sql = '''
INSERT INTO `iKnow`.`cards`
(`id`,
`nid`,
`did`,
`ord`,
`mod`,
`usn`,
`type`,
`queue`,
`due`,
`ivl`,
`factor`,
`reps`,
`lapses`,
`left`,
`odue`,
`odid`,
`flags`,
`data`)
VALUES
( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
'''

c.execute('SELECT * FROM cards')
for row in c:
    print(row[0])
    conn.execute(sql, row)

db.commit()
