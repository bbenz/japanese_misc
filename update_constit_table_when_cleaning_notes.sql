UPDATE constit c
LEFT JOIN note n ON n.keyword = c.mnem
SET c.primitive = n.kanji
WHERE c.primitive is null;
